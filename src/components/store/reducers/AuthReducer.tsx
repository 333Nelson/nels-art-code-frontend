import * as actions from "../actions";
import HttpService from "../../services/HttpService";

const initialState = {
    isLogged: false,
    user: null,
};

const check = (state) => {
    if (!!localStorage.getItem("access_token")) {
        HttpService.setDefaultHeader("Authorization", `Bearer ${localStorage.getItem("access_token")}`);

        let user = localStorage.getItem("logged_user");
        user = JSON.parse(user);

        state = Object.assign({}, state, {
            isLogged: true,
            user,
        });
    } else {
        state = Object.assign({}, state, {
            isLogged: false,
            user: null,
        });
    }

    return state;
};

const login = (state, payload) => {
    const jwtToken = payload.access_token;

    localStorage.setItem("access_token", jwtToken);
    HttpService.setDefaultHeader("Authorization", `Bearer ${jwtToken}`);

    const user = {
        id: payload.user.id,
        name: payload.user.name,
    };

    localStorage.setItem("logged_user", JSON.stringify(user));
    state = Object.assign({}, state, {
        isLogged: true,
        user,
    });

    return state;
};

const update = (state, payload) => {
    const user = JSON.parse(localStorage.getItem("logged_user"));
    const get = (key) => {
        if (payload.user.hasOwnProperty(key)) {
            return payload.user[key];
        }

        return user[key];
    };

    user.id = get("id");
    user.name = get("name");

    localStorage.setItem("logged_user", JSON.stringify(user));
    state = Object.assign({}, state, {
        isLogged: true,
        user,
    });

    return state;
};

const logout = (state) => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("logged_user");

    HttpService.removeDefaultHeader("Authorization");
    state = Object.assign({}, state, {
        isLogged: false,
        user: null,
    });

    return state;
};

export default (state = initialState, {type, payload = null}) => {
    switch (type) {
        case actions.AUTH_CHECK:
            return check(state);

        case actions.AUTH_LOGIN:
            return login(state, payload);

        case actions.AUTH_LOGOUT:
            return logout(state);

        case actions.AUTH_UPDATE:
            return update(state, payload);
    }

    return state;
};
