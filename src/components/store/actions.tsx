export const AUTH_LOGIN = "action_auth_login";
export const AUTH_LOGOUT = "action_auth_logout";
export const AUTH_CHECK = "action_auth_check";
export const AUTH_UPDATE = "action_auth_update";
