import React, {useEffect} from "react";
import { Redirect } from "react-router-dom";

import {useDispatch} from "react-redux";
import Route from "../../../Routes";
import { AUTH_LOGOUT } from "../../../store/actions";

const Logout = () => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch({
            type: AUTH_LOGOUT,
        });

    }, [])

    return <Redirect to={Route.toUrl("account_login")} />
}

export default Logout;
