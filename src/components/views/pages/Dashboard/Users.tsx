import React, {useEffect, useState} from "react";
import http from "../../../services/HttpService";
import {Table} from "reactstrap";

const Users = () => {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        http.get('/get-users')
            .then(response => response.json())
            .then((data) => {
                setUsers(data);
            });
    }, []);

    return (
        <>
            <h1>Users Page</h1>

            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Անուն</th>
                    <th>Հեռախոսահամար</th>
                    <th>Էլ․ Հասցե</th>
                </tr>
                </thead>
                <tbody>
                    {
                        users.length > 0 &&
                            users.map((item, index) => {
                                return <tr key={index}>
                                    <th scope="row">{ item.id }</th>
                                    <td>{ item.name }</td>
                                    <td>{ item.phone }</td>
                                    <td>{ item.email }</td>
                                </tr>
                            })
                    }
                </tbody>
            </Table>
        </>
    );
}

export default Users;