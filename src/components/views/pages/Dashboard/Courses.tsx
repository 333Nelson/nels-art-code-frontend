import React, {useEffect, useState} from "react";
import Course from "../../includes/Course";
import http from "../../../services/HttpService";

const Courses = () => {
    const [courses, setCourses] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        http.get('/courses')
            .then(response => response.json())
            .then((data) => {
                setCourses(data);
                setLoading(false);
            });
    }, []);

    return (
        <>
            <h1>Courses Page</h1>

            <Course courses={courses} loading={loading} url={"dashboard_single_course"} />
        </>
    );
}

export default Courses;