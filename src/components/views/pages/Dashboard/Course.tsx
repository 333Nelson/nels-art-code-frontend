import {useParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import http from "../../../services/HttpService";
import {Button, Col, Form, FormGroup, Input, Label, Row} from "reactstrap";

const Course = () => {
    const { id } : any = useParams();
    const [course, setCourse] = useState({id: 1, name: "Html & Css", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!", isDeleted: 1});

    useEffect(() => {
        http.get(`/get-course/${id}`)
            .then(response => response.json())
            .then((data) => {
                setCourse(data);
                console.log(data);
            });
    }, []);

    const handleClick = (e) => {
        e.preventDefault();
        const formData: any = new FormData();
        formData.append('id', course.id);
        formData.append('name', course.name);
        formData.append('description', course.description);

        http.withFormData('post', `/change-course`, formData)
            .then(response => response.json())
            .then((data) => {
                setCourse(data);
                console.log(data);
            });
    }

    const changeCourseStatus = () => {
        http.get(`/change-course-status/${id}`)
            .then(response => response.json())
            .then((data) => {
                setCourse(data);
            });
    }

    return (<>
        <div className={'d-flex justify-content-between'}>
            <h1>{ course.name }</h1>
            <Button color={"danger"} onClick={changeCourseStatus}>
                { course.isDeleted ? "Վերականգնել" : "Ջնջել" }
            </Button>
        </div>

        <Row>
            <Col md={12} sm={12}>
                <Form onSubmit={(e) => handleClick(e)}>
                    <Row form>
                        <Col md={12}>
                            <FormGroup>
                                <Label for={"name"}>Անվանում</Label>
                                <Input type={"text"}
                                       id={"name"}
                                       value={course.name}
                                       onChange={(e) => setCourse({...course, name: e.target.value})}
                                       name={"name"} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row form>
                        <Col md={12}>
                            <FormGroup>
                                <Label for={"description"}>Նկարագրություն</Label>
                                <Input type={"textarea"}
                                       id={"description"}
                                       value={course.description}
                                       onChange={(e) => setCourse({...course, description: e.target.value})}
                                       name={"description"} />
                            </FormGroup>
                        </Col>
                    </Row>
                    <div>
                        <FormGroup>
                            <Input type={"submit"} value={"Թարմացնել"} />
                        </FormGroup>
                    </div>
                </Form>
            </Col>
        </Row>
    </>)
}

export default Course;