import React, {useEffect, useState} from "react";
import { Jumbotron, Container } from 'reactstrap';
import img from "../../../../assets/images/courses/img1.jpg";

import {useParams} from "react-router-dom";
import http from "../../../services/HttpService";

const SingleCourse = () => {
    const { id } : any = useParams();
    const [course, setCourse] = useState({id: 1, name: "Html & Css", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!"});
    console.log(id)

    useEffect(() => {
        http.get(`/get-course/${id}`)
            .then(response => response.json())
            .then((data) => {
                // setCourse(data);
                console.log(data);
            });
    }, []);

    return (
        <div>
            <Jumbotron fluid className={"mt-2"}>
                <Container fluid className={"text-center mt-5"}>
                    <img src={img} className={"w-50"} alt="Image"/>
                    <h1 className="display-3">{course.name}</h1>
                    <h6>{course.category}</h6>
                    <p className="lead">{course.description}</p>
                </Container>
            </Jumbotron>
        </div>
    )
}

export default SingleCourse;