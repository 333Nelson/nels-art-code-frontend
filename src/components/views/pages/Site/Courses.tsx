import React, {useEffect, useState} from "react";
import Course from "../../includes/Course";
import http from "../../../services/HttpService";

/*const courses = [
    {id: 1, name: "Html & Css", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!"},
    {id: 2, name: "JS & JQuery & AJAX", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!"},
    {id: 3, name: "PHP & MySQL", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!"},
    {id: 4, name: "JAVA", category: "web", description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad facilis itaque nisi non sequi soluta!"},
];*/

const Courses = () => {
    const [courses, setCourses] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        http.get('/courses')
            .then(response => response.json())
            .then((data) => {
                setCourses(data);
                setLoading(false);
            });
    }, []);

    return (
        <>
            <h1>Courses</h1>
            <Course courses={courses} loading={loading} url={"single_course"} />
        </>
    );
}

export default Courses;