import React, {useEffect, useState} from "react";
import Course from "../../includes/Course";
import CourseRegistration from "../../includes/CourseRegistration";
import http from "../../../services/HttpService";

const HomePage = () => {
    const [courses, setCourses] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        http.get('/courses')
            .then(response => response.json())
            .then((data) => {
                setCourses(data);
                setLoading(false);
            });
    }, []);
    return (
        <>
            <h1>Home Page</h1>

            <Course courses={courses} loading={loading} />

            <CourseRegistration />
        </>
    );
}

export default HomePage;