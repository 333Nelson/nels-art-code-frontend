import React from "react";

const AboutUs = () => {
    return (
        <>
            <h1>About Us</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam, asperiores at distinctio dolore
                dolorem eaque exercitationem fugit hic ipsum libero nesciunt nisi nulla officia praesentium quaerat quam
                quibusdam quisquam quos repellendus sed sint tempora temporibus veritatis voluptatem. Animi cum deleniti
                ducimus facilis, molestias nemo praesentium reiciendis? Alias aperiam blanditiis consectetur cumque
                distinctio dolorem earum, ex expedita, labore laboriosam natus perspiciatis praesentium recusandae
                reiciendis sapiente! A assumenda at aut debitis deserunt, ex excepturi exercitationem impedit incidunt
                ipsum iure libero magni natus, nihil non odio placeat quis quisquam, recusandae reprehenderit similique
                ut voluptatibus. Accusamus adipisci alias commodi cumque dolor ducimus, ea earum explicabo facere harum
                id illum incidunt iste nostrum odio omnis quam quod ratione similique sit soluta ullam velit voluptatum.
                Alias aut culpa, cumque iste officia perferendis quasi quisquam! Commodi dignissimos error harum illo
                magnam magni nemo numquam possimus saepe ullam. Autem delectus ea excepturi nam perspiciatis totam
                vitae! Ipsam numquam saepe sapiente? Adipisci culpa deserunt dicta eos explicabo, fugiat illo illum
                impedit inventore ipsa laborum minima necessitatibus nobis nulla praesentium quia quo reiciendis sunt
                unde velit. A amet consequatur et in incidunt numquam, quidem rerum sapiente sequi vitae voluptas
                voluptate? Accusantium aperiam ea eaque exercitationem ipsam labore nam non quaerat! Aliquid amet
                blanditiis, cupiditate dolorem error in iusto libero molestias nam praesentium ratione sit soluta ullam
                ut voluptas? Aperiam consequuntur harum illo iusto numquam quae similique temporibus. Consectetur
                debitis eius maiores, mollitia nesciunt quas veniam vitae! Dolore ipsum, possimus. Cupiditate eius ex
                hic in magnam officiis possimus qui repudiandae soluta vero. Alias at cupiditate dicta expedita, facere
                inventore iste, laborum obcaecati provident quidem quisquam recusandae repellendus sit soluta tempore
                temporibus vero. Alias aut consequatur cumque dolor, dolores enim esse eveniet ex hic libero magnam
                maxime nulla perspiciatis praesentium quaerat quod ratione repudiandae tempora. Accusantium cum possimus
                quidem tempore. Consequuntur dignissimos eligendi error explicabo in inventore laudantium non officia,
                optio provident saepe, vero voluptate. Accusamus aliquid beatae consectetur dignissimos dolor dolore
                doloremque ducimus eligendi expedita fuga, id in incidunt ipsam itaque labore, maiores maxime nemo odit
                porro possimus quaerat recusandae reiciendis, sapiente voluptas voluptatum. Ab aliquam aut dicta,
                dolores et fugiat impedit nostrum numquam quasi repudiandae sit, sunt voluptate voluptatibus? A ab amet,
                architecto atque, culpa cumque fugit in ipsum laboriosam maiores neque nobis odio officiis perferendis
                porro quam sit sunt tempore veritatis vitae! Beatae dolore eos esse laborum maxime nesciunt quod ratione
                sit. Asperiores corporis dignissimos exercitationem ratione sequi! Aliquid dolor doloremque hic incidunt
                ipsa natus officia officiis repellat. Consequatur corporis cum fugiat iste itaque iure, mollitia nemo
                neque nesciunt officiis omnis quidem reiciendis reprehenderit ullam velit vero voluptatum. Culpa,
                expedita, veniam. Animi, dolorum exercitationem ipsa itaque labore obcaecati quos? Adipisci aspernatur
                dolore ipsam iste itaque iure praesentium quas reiciendis reprehenderit suscipit. Facere incidunt modi
                quod recusandae. Accusamus ad alias aliquid aperiam architecto asperiores blanditiis debitis deserunt
                doloremque excepturi in inventore ipsam iste iusto maxime minus molestias neque nihil numquam obcaecati
                omnis provident, quidem quo reprehenderit sapiente tempora voluptatum? Accusantium dolorem eius error
                est illo possimus sunt, tempore tenetur? Aperiam beatae dolorem harum non placeat? A aliquid blanditiis,
                commodi consequuntur ea earum ex, explicabo ipsam laboriosam libero modi perferendis quia quidem, quod
                recusandae reiciendis repellat sapiente sequi totam vitae? Accusantium aspernatur consectetur delectus
                dolores eaque eligendi facere illo in nam, natus officia porro sint vel? Doloribus eveniet libero odio
                quae repellendus suscipit ut. Accusantium est fuga iste natus quis sapiente. Ab, assumenda cupiditate
                deleniti ducimus facilis, nemo quia, ratione recusandae reiciendis sed vel veritatis? Accusamus ad amet
                at aut, corporis, est exercitationem incidunt laboriosam maiores modi nesciunt obcaecati odio
                perferendis provident quis reprehenderit vitae. Ab accusantium, aliquam animi atque aut culpa cupiditate
                debitis, delectus deserunt dignissimos dolore est eum fugiat inventore libero nihil pariatur perferendis
                quas quidem quod quos sint soluta unde veniam vero? Blanditiis cumque dolore ducimus excepturi
                exercitationem ipsam iste iure maiores nulla obcaecati odit officia, optio sequi soluta suscipit?
                Accusantium assumenda, at debitis dolorem doloremque earum enim eum excepturi fuga impedit in itaque
                maiores maxime molestiae odio quae, quaerat recusandae tempore voluptate voluptates? Amet, aut
                doloribus, ea eligendi excepturi laudantium maxime nulla omnis optio quis reprehenderit similique sit.
                Aspernatur earum eos esse excepturi fuga itaque natus necessitatibus perspiciatis. A ad adipisci alias
                amet architecto at atque aut, consequatur corporis delectus deleniti dolor error et facere hic id in
                incidunt labore laborum necessitatibus nemo nesciunt nisi odio possimus quam quas quidem quis sequi
                similique sit tempora velit voluptates voluptatibus. Aut corporis delectus deleniti distinctio dolorum
                ducimus ea earum error eum fugit hic ipsam minus nesciunt nobis nulla omnis quae quasi quia quidem,
                repudiandae rerum temporibus ut voluptates! Alias ex minima nihil provident veniam? Corporis dolorum
                iusto nam sed tempore ut voluptatem! A aperiam asperiores at blanditiis dicta dignissimos doloribus
                eligendi enim est facere id impedit itaque iusto, labore laborum laudantium libero minima molestias
                nihil nulla numquam odit perferendis placeat porro possimus quia quo quod repellat repudiandae rerum
                sunt suscipit ut velit vero vitae voluptas voluptatum. Accusantium cupiditate debitis dicta distinctio
                facilis id ipsam, iusto nesciunt odio perspiciatis possimus quam qui quibusdam repellat saepe velit
                voluptas! A, atque dignissimos dolores eveniet explicabo facilis id minima necessitatibus nemo, officia
                officiis, pariatur perferendis quidem tenetur totam! Asperiores, dicta esse nesciunt obcaecati qui
                quibusdam ratione? Asperiores dolorem eligendi id modi nam non quae, sint temporibus tenetur
                voluptatibus. At, corporis, quos. Alias blanditiis dignissimos dolores eveniet excepturi, hic incidunt,
                ipsam nihil numquam omnis pariatur quae quas saepe suscipit tempora totam vel voluptates. Neque ratione
                sed unde. Ad animi aperiam blanditiis dolores explicabo maxime minima modi nostrum quidem voluptas. Ab
                aspernatur, cum doloremque ducimus ea iste libero maxime minima nemo pariatur perferendis quasi, ratione
                sint! Ad doloremque, dolorum, eligendi et hic ipsam iusto laboriosam laborum nostrum quaerat quia
                recusandae sint unde vel voluptatum. Animi debitis dolores eveniet ex facere fuga magni nesciunt, odit
                porro praesentium provident rerum sequi ullam. A cumque ducimus error fuga impedit ipsum laudantium
                neque non quis voluptatem? Alias ea explicabo id illum non, quis repudiandae vero voluptas? Ab aut
                error, id minus molestias non quis repellat similique totam voluptates.</p>
        </>
    );
}

export default AboutUs;