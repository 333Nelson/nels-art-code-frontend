import React, {useState} from "react";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';
import {Link} from "react-router-dom";
import Route from "../../routing/Router";
import {connect} from "react-redux";

const DashboardMaster = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <>
            <header>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">NelsArt</NavbarBrand>
                    <NavbarToggler onClick={toggle}/>
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="me-auto" navbar>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("dashboard_my_account_page")}>Home</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("dashboard_users")}>Users</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("dashboard_courses")}>Courses</Link>
                            </NavItem>
                        </Nav>

                        <Nav className="me-auto" navbar>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>
                                    { props.loggedUser.name }
                                </DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>
                                        Option 1
                                    </DropdownItem>
                                    <DropdownItem>
                                        Option 2
                                    </DropdownItem>
                                    <DropdownItem divider />
                                    <DropdownItem>
                                        <Link to={Route.toUrl("account_logout")} className="dropdown-item notify-item">
                                            <span>Logout</span>
                                        </Link>
                                    </DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </header>
            <div className={"container mt-3"}>
                {props.children}
            </div>
            <footer>
                <p>All rights reserved. NelsArt ©</p>
            </footer>
        </>
    )
}

const mapState = (state) => {
    return {
        loggedUser: state.AuthReducer.user,
    }
}

export default connect(mapState)(DashboardMaster)