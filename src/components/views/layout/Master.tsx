import React, {useState} from "react";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import {Link} from "react-router-dom";
import Route from "../../routing/Router";
import { connect } from "react-redux";

const Master = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <>
            <header>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">NelsArt</NavbarBrand>
                    <NavbarToggler onClick={toggle}/>
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="me-auto" navbar>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("home")}>Home</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("about_us")}>About Us</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("courses")}>Courses</Link>
                            </NavItem>
                            {
                                !props.isLogged &&
                                    <NavItem>
                                        <Link className="nav-link" to={Route.toUrl("account_login")}>Login</Link>
                                    </NavItem>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </header>
            <div className={"container"}>
                {props.children}
            </div>
            <footer>
                <p>All rights reserved. NelsArt ©</p>
            </footer>
        </>
    )
}

const mapState = (state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
};

export default connect(mapState)(Master)