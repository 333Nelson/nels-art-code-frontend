import React from 'react';
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button, Row, Col
} from 'reactstrap';
import img from "../../../assets/images/courses/img1.jpg";
import Skeleton from 'react-loading-skeleton';
import {IsEmpty, Map} from 'react-lodash';
import Route from "../../routing/Router";
import {Link} from "react-router-dom";

const Loader = (props) => {
    return <Col key={props.index} md={3} sm={6} className="course">
        <Card>
            <Skeleton width={"100%"} height={200}/>
            <CardBody>
                <CardTitle tag="h5">
                    <Skeleton width={150} height={10}/>
                </CardTitle>
                <CardSubtitle tag="h6" className="mb-2 text-muted">
                    <Skeleton width={60} height={10}/>
                </CardSubtitle>
                <CardText><Skeleton width={"100%"} height={10}/></CardText>
                <Skeleton width={100} height={10}/>
            </CardBody>
        </Card>
    </Col>
}

const Course = (props) => {
    return (
        <Row>
            {
                props.loading ? [1, 2, 3, 4].map(i => <Loader index={i} />) :
                    <IsEmpty
                        value={props.courses}
                        yes="Empty list"
                        no={() => (
                            <Map collection={props.courses} iteratee={(item, index) =>
                                <Col key={index} md={3} sm={6} className="course">
                                    <Card>
                                        <CardImg top width="100%" src={img} alt="Card image cap"/>
                                        <CardBody>
                                            <CardTitle tag="h5">{item.name}</CardTitle>
                                            <CardSubtitle tag="h6" className="mb-2 text-muted">{item.category}</CardSubtitle>
                                            <CardText>{item.description}</CardText>
                                            <Link className="nav-link" to={Route.toUrl(props.url, {id: item.id})}>
                                                <Button>View</Button>
                                            </Link>
                                        </CardBody>
                                    </Card>
                                </Col>
                            } />
                        )}
                    />
            }
        </Row>
    );
}

export default Course;