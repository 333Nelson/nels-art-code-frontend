import React from 'react';
import {Form, FormGroup, Label, Input, Row, Col, CustomInput} from 'reactstrap';
import http from "../../services/HttpService";

const CourseRegistration = () => {

    const handleClick = (e) => {
        e.preventDefault();

        const formData = new FormData();
        formData.append("name", e.target.name.value);
        formData.append("category", e.target.category.value);
        formData.append("email", e.target.email.value);
        formData.append("phone", e.target.phone.value);

        http.withFormData('post','/reg', formData)
            .then(response => response.json())
            .then((data) => {
                console.log(data);
            });
    }

    return (
        <>
            <h1>Գրանցվել դասընթացին</h1>
            <Row className={"courseRegistration"}>
                <Col md={6} sm={6}>
                    <Form onSubmit={(e) => handleClick(e)}>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for={"name"}>Անուն Ազգանուն</Label>
                                    <Input type={"text"} id={"name"} name={"name"} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="category">Կատեգորիա</Label>
                                    <CustomInput type="select" id="category" name={"category"}>
                                        <option value={1}>WEB</option>
                                    </CustomInput>
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="email">Էլ․ Փոստ</Label>
                                    <Input
                                        type="text"
                                        id="email"
                                        name={"email"}
                                    />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="phone">Հեռախոսահամար</Label>
                                    <Input
                                        type="text"
                                        id="phone"
                                        name={"phone"}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <div>
                            <FormGroup>
                                <Input type={"submit"} value={"Գրանցվել"} />
                            </FormGroup>
                        </div>
                    </Form>
                </Col>
                <Col md={6} sm={6}>
                    <h1>Registration</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam atque autem debitis eligendi error illum in iusto laboriosam laudantium magnam minima molestiae nesciunt officia optio pariatur quam quidem recusandae, velit.</p>
                </Col>
            </Row>
        </>
    );
};

export default CourseRegistration;