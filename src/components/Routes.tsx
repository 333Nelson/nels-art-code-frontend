import Route from "./routing/Router";

import HomePage from "./views/pages/Site/HomePage";
import NotFound from "./views/pages/NotFound";
import LayoutMaster from "./views/layout/Master";
import LayoutDashboard from "./views/layout/DashboardMaster";

import Login from "./views/pages/Auth/Login";
import Registration from "./views/pages/Auth/Registration";
import Logout from "./views/pages/Auth/Logout";
// import Register from "./views/pages/Auth/Register";
// import VerifyAccount from "./views/pages/Auth/VerifyAccount";
// import ForgotPassword from "./views/pages/Auth/ForgotPassword";
// import ResetPassword from "./views/pages/Auth/ResetPassword";

import DashboardHome from "./views/pages/Dashboard/Home";
import DashboardUsers from "./views/pages/Dashboard/Users";
import DashboardCourses from "./views/pages/Dashboard/Courses";
import DashboardSingleCourse from "./views/pages/Dashboard/Course";

import AboutUs from "./views/pages/Site/AboutUs";
import Courses from "./views/pages/Site/Courses";
import SingleCourse from "./views/pages/Site/SingleCourse";

/* --------------------  account  --------------------- */
Route.group("/account", () => {
    Route.add("account_login", {
        path: "/login",
        component: Login,
        layout: LayoutMaster
    });

    Route.add("account_logout", {
        path: "/logout",
        component: Logout,
        //layout: AuthLayout
    });

    /*Route.add("account_forgot", {
        path: "/forgot-password",
        component: ForgotPassword,
        //layout: AuthLayout
    });

    Route.add("account_reset", {
        path: "/reset-password/:token",
        component: ResetPassword,
        //layout: AuthLayout
    });*/
});


/* --------------------  dashboard  --------------------- */
Route.group("/dashboard", () => {
    Route.redirectRoute("/", "dashboard_home");

    Route.add("dashboard_my_account_page", {
        path: "/my-account-page",
        component: DashboardHome,
        layout: LayoutDashboard,
        auth: true,
    });

    Route.add("dashboard_users", {
        path: "/users",
        component: DashboardUsers,
        layout: LayoutDashboard,
        auth: true,
    });

    Route.add("dashboard_courses", {
        path: "/courses",
        component: DashboardCourses,
        layout: LayoutDashboard,
        auth: true,
    });

    Route.add("dashboard_single_course", {
        path: "/courses/:id",
        component: DashboardSingleCourse,
        layout: LayoutDashboard,
        auth: true,
    });

});


/* --------------------  site URL  --------------------- */
Route.add("about_us", {
    path: "/about-us",
    component: AboutUs,
    layout: LayoutMaster,
});

Route.add("courses", {
    path: "/courses",
    component: Courses,
    layout: LayoutMaster,
});

Route.add("registration", {
    path: "/registration/:token",
    component: Registration,
    layout: LayoutMaster,
});

Route.add("single_course", {
    path: "/courses/:id",
    component: SingleCourse,
    layout: LayoutMaster,
});

Route.homePage(HomePage, LayoutMaster);
Route.error404(NotFound);


export default Route;